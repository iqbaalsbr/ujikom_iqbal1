<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->

  <?= validation_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">', '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  </div>') ?>
  <?= $this->session->flashdata('message'); ?>

  <div class="card o-hidden border-0 shadow-lg my-5 col-lg-7  mx-auto">
    <div class="card-body p-0">
      <!-- Nested Row within Card Body -->
      <div class="row">
        <div class="col-lg">
          <div class="p-5">
            <h1 class="h4 text-gray-900 mb-4">Register Admin</h1>

            <?= form_open('auth/registration_petugas'); ?>

            <div class="form-group">
              <label for="nama">Nama</label>
              <input type="text" class="form-control" id="nama" placeholder="" name="nama" value="<?= set_value('nama') ?>">
            </div>

            <div class="form-group">
              <label for="username">Username</label>
              <input type="text" class="form-control" id="username" name="username" placeholder="" value="<?= set_value('username') ?>">
            </div>

            <div class="form-group">
              <label for="password">Passsword</label>
              <input type="password" class="form-control" id="password" name="password" placeholder="">
            </div>
            <div class="form-group">
              <label for="telp">Telp</label>
              <input type="text" class="form-control" id="telp" placeholder="" name="telp" value="<?= set_value('telp') ?>">
            </div>

            <label for="">Level</label>
            <div class="form-group">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="level" id="admin" value="admin">
                <label class="form-check-label" for="admin">Admin</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="level" id="petugas" value="petugas" checked="">
                <label class="form-check-label" for="petugas">Petugas</label>
              </div>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
            <?= form_close(); ?>
            <hr>
            <div class="text-center">
              <a class="small" href="<?= base_url('auth/admin'); ?>">Already have an account? Login Here!</a>
              </hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



  <!-- /.container-fluid -->
</div>