<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tanggapan_m extends CI_Model
{

	private $table = 'tanggapan';
	private $primary_key = 'id_tanggapan';

	public function create($data)
	{
		return $this->db->insert($this->table, $data);
	}

	public function update($id_pengaduan, $data)
	{
		if (!$id_pengaduan) {
			return;
		}
		$this->db->set($data);
		$this->db->where('id_pengaduan', $id_pengaduan);
		return $this->db->update('pengaduan');
	}
}

/* End of file Tanggapan_m.php */
/* Location: ./application/models/Tanggapan_m.php */