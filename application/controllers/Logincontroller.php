<?php

use LDAP\Result;

defined('BASEPATH') or exit('No direct script access allowed');

class Masyarakat extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('auth_model');
        $this->load->model('profile_model');
        $this->load->model('Pengaduan_m');
    }

    public function admin()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Login page';
            $this->load->view('templates/auth_header');
            $this->load->view('auth/login_admin');
            $this->load->view('templates/auth_footer');
        } else {
            $this->login_admin();
        }
    }

    private function login_admin()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $petugas = $this->db->get_where('petugas', ['username' => $username])->row_array();

        // jika petugasnya ada
        if ($petugas) {
            // jika akun petugas == TRUE
            // cek password
            if (password_verify($password, $petugas['password'])) {
                // jika password benar
                // maka buat session userdata
                $session = [
                    'username'         => $petugas['username'],
                    'level'            => $petugas['level'],
                ];
                $this->session->set_userdata($session);
                if ($petugas['level'] == 'admin') {
                    return redirect('Admin/DashboardController');
                } elseif ($petugas['level'] == 'petugas')
                    return redirect('Admin/DashboardController');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">password salah!</div>');
                redirect('auth/admin');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">username tidak terdaftar</div>');
            redirect('auth/admin');
        }
    }

    public function username_check($str = NULL)
    {
        if (!empty($str)) :
            $masyarakat = $this->db->get_where('masyarakat', ['username' => $str])->row_array();

            $petugas = $this->db->get_where('petugas', ['username' => $str])->row_array();

            if ($masyarakat == TRUE or $petugas == TRUE) :

                $this->form_validation->set_message('username_check', 'Username ini sudah terdaftar ada.');

                return FALSE;
            else :
                return TRUE;
            endif;
        else :
            $this->form_validation->set_message('username_check', 'Inputan Kosong');

            return FALSE;
        endif;
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('password');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">You have been logged out!</div>');
        redirect('auth');
    }
}
